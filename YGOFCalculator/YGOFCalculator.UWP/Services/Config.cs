﻿using SQLite.Net.Interop;
using Windows.Storage;
using Xamarin.Forms;

[assembly: Dependency(typeof(YGOFCalculator.UWP.Config))]

namespace YGOFCalculator.UWP
{
    public class Config : IConfig
    {
        private string dbPath;
        private ISQLitePlatform platform;

        public string DbPath
        {
            get
            {
                if (string.IsNullOrEmpty(dbPath))
                {
                    dbPath = ApplicationData.Current.LocalFolder.Path;
                }
                return dbPath;
            }
        }

        public ISQLitePlatform Platform
        {
            get
            {
                if (platform == null)
                {
                    platform = new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT();
                }
                return platform;
            }
        }
    }
}
