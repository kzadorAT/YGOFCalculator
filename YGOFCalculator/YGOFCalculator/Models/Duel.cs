﻿using SQLite.Net.Attributes;
using System.Collections.Generic;

namespace YGOFCalculator
{
    public class Duel
    {
        [AutoIncrement, PrimaryKey]
        public int DuelID { get; set; }

        public int Duelformat { get; set; }

        public int PlayersQuantity { get; set; }

        public int Winner { get; set; }

        public List<User> Players { get; set; }

        public Duel()
        {
            Players = new List<User>();            
        }

        // Methods
        public User GetWinner()
        {
            return Players[Winner];
        }
    }
}
