﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YGOFCalculator
{
    public class User
    {
        [AutoIncrement, PrimaryKey]
        public int UserID { get; set; }

        public string Name { get; set; }

        public int Wins { get; set; }

        public int Losses { get; set; }

        public List<Duel> DuelsHistory { get; set; }

        public List<int> LifePoints { get; set; }

        // Methods
        public User()
        {
            LifePoints = new List<int>()
            {
                0,
            };

        }
    }
}
