﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace YGOFCalculator
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DuelOptionsPage : ContentPage
    {
        DuelOptionsVM context;
        public DuelOptionsPage()
        {
            context = new DuelOptionsVM(Navigation);
            InitializeComponent();

            BindingContext = context;
        }

        private void ToggledTo16k(object sender, ToggledEventArgs e)
        {

            if (((Switch)sender).IsToggled)
            {
                context.SelectedLifePoints = 16000;
            }
            else
            {
                context.SelectedLifePoints = 8000;
            }
        }

        private void SelectedIndexChanged(object sender, System.EventArgs e)
        {
            var pick = (Picker)sender;
            switch (pick.SelectedIndex)
            {
                case 2:
                    context.SelectedLifePoints = 16000;
                    break;
                case 5:
                    context.SelectedLifePoints = 24000;
                    break;
                default:
                    context.SelectedLifePoints = 8000;
                    break;
            }
        }
    }
}