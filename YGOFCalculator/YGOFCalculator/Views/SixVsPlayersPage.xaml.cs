﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace YGOFCalculator
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SixVsPlayersPage : ContentPage
    {
        DisplaysVM context;
        public SixVsPlayersPage(Duel duel)
        {
            context = new DisplaysVM(Navigation, duel);
            InitializeComponent();
            BindingContext = context;
            if (duel.Duelformat == 5)
            {
                LifeGrid.RowDefinitions.Clear();
                LifeGrid.ColumnDefinitions.Clear();
                LifeGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.05, GridUnitType.Star) });
                LifeGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.45, GridUnitType.Star) });
                LifeGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.45, GridUnitType.Star) });
                LifeGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.05, GridUnitType.Star) });
                LifeGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                LifeGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                LifeGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                LifeGrid.Children.Remove(Team2Display);
                LifeGrid.Children.Add(Team2Display, 0, 1);
                Grid.SetColumnSpan(Team2Display, 3);
                LifeGrid.Children.Remove(Team1Display);
                LifeGrid.Children.Add(Team1Display, 0, 2);
                Grid.SetColumnSpan(Team1Display, 3);
                LifeGrid.Children.Remove(Display3);
                LifeGrid.Children.Remove(Display4);
                LifeGrid.Children.Remove(Display5);
                LifeGrid.Children.Remove(Display6);
                Display3 = null;
                Display4 = null;
                Display5 = null;
                Display6 = null;
            }
        }
    }
}