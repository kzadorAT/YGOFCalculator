﻿using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace YGOFCalculator
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CalculatorPage : ContentPage
	{
        public CalculatorVM context;
        private double width = 0;
        private double height = 0;

		public CalculatorPage (User user, int rotateCalculator)
		{
            context = new CalculatorVM(Navigation, user);
            this.RotateTo(rotateCalculator, 2000, Easing.CubicOut);
            InitializeComponent ();
            actualLP.Text = user.LifePoints.Last().ToString();
            Title = user.Name;
            BindingContext = context;
		}

        public CalculatorPage()
        {
            InitializeComponent();
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);
            if(width != this.width || height != this.height)
            {
                this.width = width;
                this.height = height;
                if(width > height) // Landscape
                {
                    GridButtons.RowDefinitions.Clear();
                    GridButtons.ColumnDefinitions.Clear();
                    GridButtons.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                    GridButtons.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                    GridButtons.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                    GridButtons.Children.Remove(HalfButton);
                    GridButtons.Children.Add(HalfButton,1,0);
                }
                else // Portrait
                {
                    GridButtons.RowDefinitions.Clear();
                    GridButtons.ColumnDefinitions.Clear();
                    GridButtons.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                    GridButtons.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                    GridButtons.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                    GridButtons.Children.Remove(HalfButton);
                    GridButtons.Children.Add(HalfButton, 0, 1);
                }
            }
        }
    }
}