﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace YGOFCalculator
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TwoPlayersPage : ContentPage
	{
        DisplaysVM context;
		public TwoPlayersPage (Duel duel)
		{
            context = new DisplaysVM(Navigation, duel);
			InitializeComponent ();
            BindingContext = context;
		}
	}
}