﻿using System.Collections.Generic;

namespace YGOFCalculator
{
    class HistoryPageVM : ObservableBaseObject
    {
        private List<HistoryLP> lifePoints;
        private string playerName;

        public List<HistoryLP> LifePoints
        {
            get { return lifePoints; }
            set { lifePoints = value; OnPropertyChanged(); }
        }

        public string PlayerName
        {
            get { return playerName; }
            set { playerName = value; OnPropertyChanged(); }
        }

        public HistoryPageVM(User user)
        {
            LifePoints = new List<HistoryLP>();
            for (int i = 0; i < user.LifePoints.Count; i++)
            {
                HistoryLP historyLP = new HistoryLP();
                historyLP.Difference = 0;
                historyLP.LifePoint = user.LifePoints[i];
                if(i != 0)
                {
                    historyLP.Difference = user.LifePoints[i] - user.LifePoints[i - 1];
                }
                LifePoints.Add(historyLP);
            }
        }
    }

    public class HistoryLP
    {
        public int LifePoint { get; set; }
        public int Difference { get; set; }
    }
}
