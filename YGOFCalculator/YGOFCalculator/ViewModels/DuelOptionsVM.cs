﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace YGOFCalculator
{
    public class DuelOptionsVM : ObservableBaseObject
    {
        private List<string> gameModes;
        private int selectedGameMode;
        private INavigation page;
        public ICommand StartCommand { get; set; }

        public List<string> GameModes
        {
            get { return gameModes; }
            set { gameModes = value; OnPropertyChanged(); }
        }

        public int SelectedGameMode
        {
            get { return selectedGameMode; }
            set { selectedGameMode = value; OnPropertyChanged(); }
        }

        private int selectedLifePoints;

        public int SelectedLifePoints
        {
            get { return selectedLifePoints; }
            set { selectedLifePoints = value; OnPropertyChanged(); }
        }



        public DuelOptionsVM(INavigation page)
        {
            this.page = page;
            GameModes = Library.GameModes;
            SelectedGameMode = 0;
            SelectedLifePoints = 8000;
            StartCommand = new Command(() => Start());
        }

        private async void Start()
        {
            Duel duel = new Duel
            {
                Duelformat = SelectedGameMode
            };
            if(SelectedLifePoints < 4000)
            {
                SelectedLifePoints = 8000;
            }
            // 1 vs 1 Duel format - NORMAL
            User player1 = new User();
            player1.Name = "Alvaro";
            player1.LifePoints[0] = SelectedLifePoints;
            User player2 = new User();
            player2.Name = "Daniel";
            player2.LifePoints[0] = SelectedLifePoints;
            duel.Players.Add(player1);
            duel.Players.Add(player2);
            if (GameModes[SelectedGameMode] == Library.GameModes[0])
            {
                
                await page.PushAsync(new TwoPlayersPage(duel));
            }
            else
            {
                User player3 = new User();
                player3.Name = "Fabricio";
                player3.LifePoints[0] = SelectedLifePoints;
                duel.Players.Add(player3);
                if (GameModes[SelectedGameMode] == Library.GameModes[1])
                {
                    // 3vs Duel format - 3VS
                    // await page.PushAsync(new ThreePlayersPage());
                }
                else
                {
                    User player4 = new User();
                    player4.Name = "Francisco";
                    player4.LifePoints[0] = SelectedLifePoints;
                    duel.Players.Add(player4);
                    if (GameModes[SelectedGameMode] == Library.GameModes[2])
                    {
                        // 2 vs 2 Duel format - Tag
                        await page.PushAsync(new TagDuelPage(duel));
                    }
                    else if (GameModes[SelectedGameMode] == Library.GameModes[3])
                    {
                        // 4vs Duel format - 4VS
                        await page.PushAsync(new TagDuelPage(duel)); // TODO change design for 4 versus players
                    }
                    else
                    {
                        User player5 = new User();
                        duel.Players.Add(player5);
                        player5.Name = "Matias";
                        player5.LifePoints[0] = SelectedLifePoints;
                        if (GameModes[SelectedGameMode] == Library.GameModes[4])
                        {
                            // 5vs Duel Format - 5VS
                            // await page.PushAsync(new FivePlayersPage());
                        }
                        else
                        {
                            User player6 = new User();
                            player6.Name = "Rodrigo";
                            player6.LifePoints[0] = SelectedLifePoints;
                            duel.Players.Add(player6);
                            if (GameModes[SelectedGameMode] == Library.GameModes[5])
                            {
                                // 3 vs 3 Duel Format - 3 VS 3
                                await page.PushAsync(new SixVsPlayersPage(duel)); // TODO change design for 3 versus players
                            }
                            else if (GameModes[SelectedGameMode] == Library.GameModes[6])
                            {
                                // 6vs Duel Format - 6VS
                                await page.PushAsync(new SixVsPlayersPage(duel));
                            }
                        }
                    }
                }
            }
        }
    }
}
