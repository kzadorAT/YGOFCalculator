﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace YGOFCalculator
{
    public class DisplaysVM : ObservableBaseObject
    {
        private INavigation page;
        private Duel duel;

        // Displays
        private string display1;
        private bool display1Enable;
        private string player1Name;
        private string display2;
        private bool display2Enable;
        private string player2Name;
        private string display3;
        private bool display3Enable;
        private string player3Name;
        private string display4;
        private bool display4Enable;
        private string player4Name;
        private string display5;
        private bool display5Enable;
        private string player5Name;
        private string display6;
        private bool display6Enable;
        private string player6Name;

        // Commands
        public ICommand CalculateCommand { get; set; }

        // Displays

        public string Display1
        {
            get { return display1; }
            set { display1 = value; OnPropertyChanged(); }
        }
        public bool Display1Enable
        {
            get { return display1Enable; }
            set { display1Enable = value; OnPropertyChanged(); }
        }
        public string Player1Name
        {
            get { return player1Name; }
            set { player1Name = value; OnPropertyChanged(); }
        }

        public string Display2
        {
            get { return display2; }
            set { display2 = value; OnPropertyChanged(); }
        }
        public bool Display2Enable
        {
            get { return display2Enable; }
            set { display2Enable = value; OnPropertyChanged(); }
        }
        public string Player2Name
        {
            get { return player2Name; }
            set { player2Name = value; OnPropertyChanged(); }
        }

        public string Display3
        {
            get { return display3; }
            set { display3 = value; OnPropertyChanged(); }
        }
        public bool Display3Enable
        {
            get { return display3Enable; }
            set { display3Enable = value; OnPropertyChanged(); }
        }
        public string Player3Name
        {
            get { return player3Name; }
            set { player3Name = value; OnPropertyChanged(); }
        }

        public string Display4
        {
            get { return display4; }
            set { display4 = value; OnPropertyChanged(); }
        }
        public bool Display4Enable
        {
            get { return display4Enable; }
            set { display4Enable = value; OnPropertyChanged(); }
        }
        public string Player4Name
        {
            get { return player4Name; }
            set { player4Name = value; OnPropertyChanged(); }
        }

        public string Display5
        {
            get { return display5; }
            set { display5 = value; OnPropertyChanged(); }
        }
        public bool Display5Enable
        {
            get { return display5Enable; }
            set { display5Enable = value; OnPropertyChanged(); }
        }
        public string Player5Name
        {
            get { return player5Name; }
            set { player5Name = value; OnPropertyChanged(); }
        }

        public string Display6
        {
            get { return display6; }
            set { display6 = value; OnPropertyChanged(); }
        }
        public bool Display6Enable
        {
            get { return display6Enable; }
            set { display6Enable = value; OnPropertyChanged(); }
        }
        public string Player6Name
        {
            get { return player6Name; }
            set { player6Name = value; OnPropertyChanged(); }
        }

        public DisplaysVM(INavigation page, Duel duel)
        {
            this.page = page;

            this.duel = duel;

            // Enable Player1 and Player2
            Display1Enable = true;
            Display2Enable = true;
            Display1 = this.duel.Players[0].LifePoints.Last().ToString();
            Display2 = this.duel.Players[1].LifePoints.Last().ToString();
            Player1Name = this.duel.Players[0].Name;
            Player2Name = this.duel.Players[1].Name;

            //Enable Player3 for 3vs - Tag - 4vs - 5vs - 3vs3 - 6vs
            if (duel.Duelformat == 1 || duel.Duelformat == 2 ||
                duel.Duelformat == 3 || duel.Duelformat == 4 ||
                duel.Duelformat == 5 || duel.Duelformat == 6)
            {
                if(duel.Duelformat != 2 && duel.Duelformat != 5)
                {
                    Display3Enable = true;
                    Display3 = this.duel.Players[2].LifePoints.Last().ToString();
                }
                Player3Name = this.duel.Players[2].Name;
            }

            //Enable Player4 for Tag - 4vs - 5vs - 3vs3 - 6vs
            if (duel.Duelformat == 2 ||
                duel.Duelformat == 3 || duel.Duelformat == 4 ||
                duel.Duelformat == 5 || duel.Duelformat == 6)
            {
                if (duel.Duelformat != 2 && duel.Duelformat != 5)
                {
                    Display4Enable = true;
                    Display4 = this.duel.Players[3].LifePoints.Last().ToString();
                }
                Player4Name = this.duel.Players[3].Name;
            }

            //Enable Player5 for 5vs - 3vs3 - 6vs
            if (duel.Duelformat == 4 ||
                duel.Duelformat == 5 || duel.Duelformat == 6)
            {
                if (duel.Duelformat != 5)
                {
                    Display5Enable = true;
                    Display5 = this.duel.Players[4].LifePoints.Last().ToString();
                }
                Player5Name = this.duel.Players[4].Name;
            }

            //Enable Player6 for 3vs3 - 6vs
            if (duel.Duelformat == 5 || duel.Duelformat == 6)
            {
                if (duel.Duelformat != 5)
                {
                    Display6Enable = true;
                    Display6 = this.duel.Players[5].LifePoints.Last().ToString();
                }
                Player6Name = this.duel.Players[5].Name;
            }

            CalculateCommand = new Command<string>((string parameter) => CalculateLP(parameter));
        }

        private async void CalculateLP(string parameter)
        {
            User user;
            int rotateCalculator = 0;
            switch (parameter)
            {
                case "Player2":
                    user = this.duel.Players[1];
                    rotateCalculator = 180;
                    break;
                case "Player3":
                    user = this.duel.Players[2];
                    break;
                case "Player4":
                    user = this.duel.Players[3];
                    rotateCalculator = 180;
                    break;
                case "Player5":
                    user = this.duel.Players[4];
                    break;
                case "Player6":
                    user = this.duel.Players[5];
                    rotateCalculator = 180;
                    break;
                default:
                    user = this.duel.Players[0];
                    break;
            }
            CalculatorPage newPage = new CalculatorPage(user, rotateCalculator);
            await page.PushAsync(newPage);
            newPage.Disappearing += NewPage_Disappearing;
        }

        private async void NewPage_Disappearing(object sender, EventArgs e)
        {
            var returnedPage = sender as CalculatorPage;
            User returnedUser = returnedPage.context.user;
            if(returnedUser.LifePoints.Last() < 1)
            {
                returnedUser.LifePoints[returnedUser.LifePoints.Count - 1] = 0;
            }
            if (this.duel.Players[0].Name == returnedUser.Name)
            {
                this.duel.Players[0].LifePoints = returnedUser.LifePoints;
                Display1 = this.duel.Players[0].LifePoints.Last().ToString();
                if (this.duel.Players[0].LifePoints.Last() == 0 && await ShowDefeatMessage())
                {
                    Display1Enable = false;
                }
                return;
            }
            if (this.duel.Players[1].Name == returnedUser.Name)
            {
                this.duel.Players[1].LifePoints = returnedUser.LifePoints;
                Display2 = this.duel.Players[1].LifePoints.Last().ToString();
                if (this.duel.Players[1].LifePoints.Last() == 0 && await ShowDefeatMessage())
                {
                    Display2Enable = false;
                }
                return;
            }
            if (this.duel.Players[2].Name == returnedUser.Name)
            {
                this.duel.Players[2].LifePoints = returnedUser.LifePoints;
                Display3 = this.duel.Players[2].LifePoints.Last().ToString();
                if (this.duel.Players[2].LifePoints.Last() == 0 && await ShowDefeatMessage())
                {
                    Display3Enable = false;
                }
                return;
            }
            if (this.duel.Players[3].Name == returnedUser.Name)
            {
                this.duel.Players[3].LifePoints = returnedUser.LifePoints;
                Display4 = this.duel.Players[3].LifePoints.Last().ToString();
                if (this.duel.Players[3].LifePoints.Last() == 0 && await ShowDefeatMessage())
                {
                    Display4Enable = false;
                }
                return;
            }
            if (this.duel.Players[4].Name == returnedUser.Name)
            {
                this.duel.Players[4].LifePoints = returnedUser.LifePoints;
                Display5 = this.duel.Players[4].LifePoints.Last().ToString();
                if (this.duel.Players[4].LifePoints.Last() == 0 && await ShowDefeatMessage())
                {
                    Display5Enable = false;
                }
                return;
            }
            if (this.duel.Players[5].Name == returnedUser.Name)
            {
                this.duel.Players[5].LifePoints = returnedUser.LifePoints;
                Display6 = this.duel.Players[5].LifePoints.Last().ToString();
                if (this.duel.Players[5].LifePoints.Last() == 0 && await ShowDefeatMessage())
                {
                    Display6Enable = false;
                }
                return;
            }
        }

        private async Task<bool> ShowDefeatMessage()
        {
            DialogService dialog = new DialogService();
            return await dialog.ShowConfirm("Your life points reach 0", "Confirm that you have been defeated");
        }
    }
}
