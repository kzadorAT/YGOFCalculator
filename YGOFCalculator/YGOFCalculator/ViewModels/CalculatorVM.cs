﻿using Rg.Plugins.Popup.Services;
using System;
using System.Windows.Input;
using Xamarin.Forms;
using YGOFCalculator.Views;

namespace YGOFCalculator
{
    public class CalculatorVM : ObservableBaseObject
    {
        #region Private Members

        private bool isOperationDisplayed;
        private INavigation page;
        public  User user;

        // Bindings
        private string currentEntry;
        private string historyString;

        #endregion

        #region VM Properties

        // Commands
        public ICommand NumericCommand { get; set; }
        public ICommand ClearCommand { get; set; }
        public ICommand BackSpaceCommand { get; set; }
        public ICommand AddCommand { get; set; }
        public ICommand SubtractCommand { get; set; }
        public ICommand HalfCommand { get; set; }
        public ICommand DoubleCommand { get; set; }
        public ICommand ClearEntryCommand { get; set; }
        public ICommand SurrenderCommand { get; set; }
        public ICommand HistoryCommand { get; set; }
        public ICommand UndoCommand { get; set; }
        public ICommand RedoCommand { get; set; }
        // Bindings
        public string CurrentEntry
        {
            get { return currentEntry; }
            set { currentEntry = value; OnPropertyChanged(); }
        }

        public string HistoryString
        {
            get { return historyString; }
            set { historyString = value; OnPropertyChanged(); }
        }

        private bool undoIsEnabled;

        public bool UndoIsEnabled
        {
            get { return undoIsEnabled; }
            set { undoIsEnabled = value; OnPropertyChanged(); }
        }

        #endregion

        #region Costructor

        public CalculatorVM(INavigation page, User user)
        {
            this.page = page;
            this.user = user;
            NumericCommand = new Command<string>((string parameter) => NumericPressed(parameter));
            ClearCommand = new Command(() => Clear());
            BackSpaceCommand = new Command(() => BackSpacePressed());
            AddCommand = new Command(() => Add());
            SubtractCommand = new Command(() => Subtract());
            HalfCommand = new Command(() => Half());
            DoubleCommand = new Command(() => Double());
            HistoryCommand = new Command(() => History());
            SurrenderCommand = new Command(() => Surrender());
            UndoCommand = new Command(() => Undo());

            CurrentEntry = "0";
            isOperationDisplayed = true;
            CheckEnabled();
        }


        #endregion

        #region Methods

        private void NumericPressed(string parameter)
        {
            if (isOperationDisplayed || CurrentEntry == "0")
            {
                CurrentEntry = parameter;
            }
            else
            {
                CurrentEntry += parameter;
            }
            isOperationDisplayed = false;
        }

        private void Clear()
        {
            HistoryString = "";
            CurrentEntry = "0";
            isOperationDisplayed = false;
        }

        private void BackSpacePressed()
        {
            CurrentEntry = CurrentEntry.Substring(0, CurrentEntry.Length - 1);

            if(CurrentEntry.Length == 0)
            {
                CurrentEntry = "0";
            }
        }

        private void Add()
        {
            user.LifePoints.Add(user.LifePoints[user.LifePoints.Count -1] + int.Parse(CurrentEntry));
            page.PopAsync();
        }

        private void Subtract()
        {
            user.LifePoints.Add(user.LifePoints[user.LifePoints.Count - 1] - int.Parse(CurrentEntry));
            page.PopAsync();
        }

        private void Half()
        {
            user.LifePoints.Add(user.LifePoints[user.LifePoints.Count - 1] / 2);
            page.PopAsync();
        }

        private void Double()
        {
            user.LifePoints.Add(user.LifePoints[user.LifePoints.Count - 1] * 2);
            page.PopAsync();
        }

        private void Surrender()
        {
            user.LifePoints.Add(0);
            page.PopAsync();
        }

        private async void History()
        {
            HistoryPage historyPage = new HistoryPage(user);
            await PopupNavigation.PushAsync(historyPage);
        }

        private void Undo()
        {
            user.LifePoints.RemoveAt(user.LifePoints.Count - 1);
            CheckEnabled();
            page.PopAsync();
        }

        private void CheckEnabled()
        {
            if(user.LifePoints.Count != 1)
            {
                UndoIsEnabled = true;
            }
            else
            {
                undoIsEnabled = false;
            }
        }

        #endregion


    }
}
