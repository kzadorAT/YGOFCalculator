﻿using System.Collections.Generic;

namespace YGOFCalculator
{
    public class Library
    {
        public static List<string> GameModes = new List<string>()
        {
            "Normal",
            "3vs",
            "Tag",
            "4vs",
            "5vs",
            "3 vs 3",
            "6vs",
        };
    }
}
