﻿using SQLite.Net.Interop;

namespace YGOFCalculator
{
    public interface IConfig
    {
        string DbPath { get; }

        ISQLitePlatform Platform { get; }
    }
}
