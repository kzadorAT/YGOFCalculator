﻿using SQLite.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace YGOFCalculator
{
    public class DataAccess : IDisposable
    {
        private SQLiteConnection userConnection;
        private SQLiteConnection duelsConnection;

        public DataAccess()
        {
            var config = DependencyService.Get<IConfig>();
            userConnection = new SQLiteConnection(config.Platform, System.IO.Path.Combine(config.DbPath, "user.db3"));
            duelsConnection = new SQLiteConnection(config.Platform, System.IO.Path.Combine(config.DbPath, "duels.db3"));
            userConnection.CreateTable<User>();
            duelsConnection.CreateTable<Duel>();
        }

        #region UserBDActions
        public void InsertUser(User user)
        {
            userConnection.Insert(user);
        }

        public void UpdateUser(User user)
        {
            userConnection.Update(user);
        }

        public void DeleteUser(User user)
        {
            userConnection.Delete(user);
        }

        public User GetUser(int UserID)
        {
            return userConnection.Table<User>().FirstOrDefault(c => c.UserID == UserID);
        }

        public List<User> GetUsers()
        {
            return userConnection.Table<User>().OrderBy(c => c.Name).ToList();
        }
        #endregion

        #region DuelsBDActions
        public void InsertDuel(Duel duel)
        {
            userConnection.Insert(duel);
        }

        public void UpdateUser(Duel duel)
        {
            userConnection.Update(duel);
        }

        public void DeleteUser(Duel duel)
        {
            userConnection.Delete(duel);
        }

        public Duel GetDuel(int DuelID)
        {
            return userConnection.Table<Duel>().FirstOrDefault(c => c.DuelID == DuelID);
        }

        public List<Duel> GetDuels()
        {
            return userConnection.Table<Duel>().OrderBy(c => c.Duelformat).ToList();
        }
        #endregion


        public void Dispose()
        {
            if(userConnection != null)
            {
                userConnection.Dispose();
            }
            if(duelsConnection != null)
            {
                duelsConnection.Dispose();
            }
        }
    }
}
