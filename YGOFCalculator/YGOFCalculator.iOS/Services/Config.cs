﻿using SQLite.Net.Interop;
using System;
using Xamarin.Forms;

[assembly: Dependency(typeof(YGOFCalculator.iOS.Config))]

namespace YGOFCalculator.iOS
{
    public class Config : IConfig
    {
        private string dbPath;
        private ISQLitePlatform platform;

        public string DbPath
        {
            get
            {
                if (string.IsNullOrEmpty(dbPath))
                {
                    var path = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                    dbPath = System.IO.Path.Combine(path, "..", "Library");
                }

                return dbPath;
            }
        }

        public ISQLitePlatform Platform
        {
            get
            {
                if (platform == null)
                {
                    platform = new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS();

                }

                return platform;
            }
        }
    }
}
