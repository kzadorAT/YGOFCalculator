﻿using SQLite.Net.Interop;
using Xamarin.Forms;

[assembly: Dependency(typeof(YGOFCalculator.Droid.Config))]

namespace YGOFCalculator.Droid
{
    public class Config : IConfig
    {
        private string dbPath;
        private ISQLitePlatform platform;

        public string DbPath
        {
            get
            {
                if (string.IsNullOrEmpty(dbPath))
                {
                    dbPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                }

                return dbPath;
            }
        }

        public ISQLitePlatform Platform
        {
            get
            {
                if (platform == null)
                {
                    platform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
                }

                return platform;
            }
        }
    }
}